﻿#========================================================================
# Created with: SAPIEN Technologies, Inc., PowerShell Studio 2012 v3.1.29
# Created on:   
# Created by:   Paul DeArment Jr
# Organization:  
# Filename:     Get-ProfileSizesWithPrinters
#requirements - download du from sysinternals
#========================================================================
<#
	.SYNOPSIS
		This script allows you to get the size of roaming profiles along with the number of printers stored in the registry for each profile
	
	.DESCRIPTION
		This script allows you to get the size of roaming profiles along with the number of printers stored in the registry for each profile
	
	.PARAMETER ProfileShare
		The path to where the profile shares are stored
	
	.PARAMETER ProfileOutputFile
		The path to save the Profile CSV File
	
	.PARAMETER TempNtLocation
		The location to temporarily store the ntdata.dat files.  This will be in the format of \\Profileshare\TempNtLocation
	
	.PARAMETER PrinterOutputFile
		The path to store the Printer CSV File
	
	.PARAMETER CombinedPrinterProfileOutput
		The path to store the combined printer and profile CSV File
	
	.PARAMETER DULocation
		The path to the DU EXE
	
	.EXAMPLE
		PS C:\> profileSizes.ps1 -ProfileShare '\\clientfs\profiles$' -ProfileOutputFile 'c:\information\ClientProfiles.csv' -PrinterOutputFile 'c:\information\ClientPrinters.csv' -combinedprinterprofileoutput 'c:\information\ClientCombined.csv' -DuLocation "c:\du\du.exe"
	
	.NOTES
		Additional information about the file.
#>
[CmdletBinding(ConfirmImpact = 'None',
			   PositionalBinding = $false,
			   SupportsPaging = $false,
			   SupportsShouldProcess = $false)]
param
(
	[string]$ProfileShare,
	[string]$ProfileOutputFile,
	[string]$TempNtLocation,
	[string]$PrinterOutputFile,
	[string]$CombinedPrinterProfileOutput,
	[string]$DULocation
)


#where to temporarily store ntuser.dat files
$ntlocation = "$ProfileShare\$($TempNtLocation)"

Remove-Item $ProfileOutputFile -ErrorAction 'Continue'
Remove-Item $PrinterOutputFile -ErrorAction 'Continue'
Remove-Item $CombinedPrinterProfileOutput -ErrorAction 'Continue'
Write-Host "If no file present, you will receive an error"

foreach ($item in gci $ProfileShare)
{
	#adjust path for where DU is located
	$Command = "& $($DULocation) -c `"$($ProfileShare)\$($item.name)`" >> $($ProfileOutputFile)"
	Invoke-Expression $Command
}

foreach ($item in gci $ProfileShare)
{
	Copy-Item "$ProfileShare\$($item.name)\upm\upm_profile\ntuser.dat" -Destination "$ntlocation\$($item.name).dat"
}


##suggestion is to run this from local machine or client server which will not impact client performance - the registry will be used ALOT
foreach ($item in gci $ntlocation)
{
	reg load 'HKLM\TempUser' $item.FullName
	$count = (gci hklm:\tempuser\printers | select -ExpandProperty property).count
	$stringData = $null
	$stringData = New-Object psobject
	$stringData | Add-Member -MemberType 'NoteProperty' -Name printerCount -Value $count
	$stringData | Add-Member -MemberType 'NoteProperty' -Name profileName -Value $item.Name.Replace(".dat", "")
	$stringData
	$stringData | Export-Csv -Path $PrinterOutputFile -Append -NoTypeInformation
	[gc]::collect()
	reg unload hklm\TempUser
}


$printerdata = Import-Csv $PrinterOutputFile
$data = foreach ($item in Import-Csv $ProfileOutputFile | ?{ $_.path -ne "Path" })
{
	$selectedPrinterCount = $printerdata | ?{ $_.profilename -eq $item.path.replace($ProfileShare, "").replace("\", "") }
	$obj = new-object psobject
	$obj | add-member -membertype noteproperty -name path -value $item.path
	$obj | add-member -membertype noteproperty -name FileCount -value $item.FileCount
	$obj | add-member -membertype noteproperty -name DirectoryCount -value $item.DirectoryCount
	$obj | add-member -membertype noteproperty -name DirectorySize_KB -value $item.DirectorySize
	$obj | add-member -membertype noteproperty -name DirectorySize_MB -value (([int]$item.DirectorySize)/1024)
	$obj | add-member -membertype noteproperty -name PrinterCount -value $selectedPrinterCount.printerCount
	$obj
}

$data | Export-Csv $CombinedPrinterProfileOutput -NoTypeInformation


  